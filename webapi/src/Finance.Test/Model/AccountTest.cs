﻿using System;
using System.Text;
using System.Collections.Generic;
using Finance.Domain.Model;
using Xunit;

namespace Finance.Test.Model
{
    /// <summary>
    /// Summary description for AccountTest
    /// </summary>
    
    public class AccountTest
    {
        public const string UserId = "faf39f35-4e0a-4850-8461-a24077726114";

        public AccountTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        /// <summary>
        ///A test for NewAccount
        ///</summary>
        [Fact]
        public void NewAccountTest()
        {
            string bankName = "033";
            string agency = "4521"; 
            string accountNumber = "048975-5"; 
            string description = "Conta Principal"; 
            Account target = new Account(bankName, agency, accountNumber, description, AccountTest.UserId);
            
            Assert.Equal(bankName, target.BankName);
            Assert.Equal(agency, target.AccountAgency);
            Assert.Equal(accountNumber, target.AccountNumber);
            Assert.Equal(description, target.AccountDescription);
            Assert.Equal(0, target.BalanceValue);

        }

        /// <summary>
        ///A test for ChangeMoviment
        ///</summary>
        [Fact]
        public void ChangeMovimentTest()
        {
            string bankName = "033";
            string agency = "4521";
            string accountNumber = "048975-5";
            string description = "Conta Principal";
            Account target = new Account(bankName, agency, accountNumber, description, AccountTest.UserId);


            Decimal value = 256.26m; 
            DateTime transactionDate = new DateTime(2009, 02, 05); 
            string TransactionDescription = "Descricao 1"; 
            bool AccountTransfer = false; 
            bool capitalization = false; 


            Transaction currentTransaction = target.NewTransaction(123.45m, new DateTime(2005, 10, 14), "TEste", false, false, null, AccountTest.UserId);
            currentTransaction = target.NewTransaction(12.45m, new DateTime(2005, 10, 14), "TEste", false, false, null, AccountTest.UserId);
            currentTransaction = target.NewTransaction(-98.95m, new DateTime(2005, 10, 14), "TEste", false, false, null, AccountTest.UserId);


            Transaction actualTransaction = target.ChangeMoviment(currentTransaction, value, transactionDate, TransactionDescription, capitalization, false);



            Assert.Equal(value, actualTransaction.TransactionValue);
            Assert.Equal(transactionDate, actualTransaction.TransactionDate);
            Assert.Equal(TransactionDescription, actualTransaction.TransactionDescription);
            Assert.Equal(AccountTransfer, actualTransaction.AccountTransfer);
            Assert.Equal(capitalization, actualTransaction.Capitalization);

            Assert.Equal((123.45m + 12.45m - 98.95m) + 98.95m + 256.26m, target.BalanceValue);

        }

        /// <summary>
        ///A test for NewTransaction
        ///</summary>
        [Fact]
        public void NewTransactionTest()
        {
            Account target = new Account();
            Decimal _Valor = 235.46m; 
            DateTime _Dt_Movimento = new DateTime(2009, 02, 05); 
            string _Descricao = "Descricao 1"; 
            bool _Capitalizacao = false; 

            Transaction actual;
            actual = target.NewTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false, null, AccountTest.UserId);
            Assert.Equal(_Valor, target.BalanceValue);

            actual = target.NewTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false, null, AccountTest.UserId);
            Assert.Equal(_Valor * 2, target.BalanceValue);


            actual = target.NewTransaction(-56.36m, _Dt_Movimento, _Descricao, _Capitalizacao, false, null, AccountTest.UserId);
            Assert.Equal((_Valor * 2) - 56.36m, target.BalanceValue);

        }

        /// <summary>
        ///A test for RemoveTransaction
        ///</summary>
        [Fact]
        public void RemoveTransaction()
        {
            Account target = new Account();
            Transaction currentTransaction = new Transaction();

            target.Transaction.Add(currentTransaction);
            target.Transaction.Add(new Transaction());
            Transaction actual = target.RemoveMoviment(currentTransaction);

            Assert.Equal(1, target.Transaction.Count);
        }
    }
}
