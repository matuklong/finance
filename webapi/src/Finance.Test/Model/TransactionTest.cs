﻿using System;
using System.Text;
using System.Collections.Generic;
using Finance.Domain.Model;
using Xunit;

namespace Finance.Test.Model
{
    /// <summary>
    /// Summary description for AccountTest
    /// </summary>
    public class TransactionTest
    {

        public TransactionTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        ///A test for Altera_Movimento
        ///</summary>
        [Fact]
        public void Altera_MovimentoTest()
        {
            Transaction target = new Transaction();
            Decimal _Valor = 256.26m;
            DateTime _Dt_Movimento = new DateTime(2009, 02, 05);
            string _Descricao = "Descricao 1";

            bool _Creditado = false;
            bool _Capitalizacao = false;

            target.ChangeTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false);

            Assert.Equal(_Valor, target.TransactionValue);
            Assert.Equal(_Dt_Movimento, target.TransactionDate);
            Assert.Equal(_Descricao, target.TransactionDescription);
            Assert.Equal(_Creditado, target.AccountTransfer);
            Assert.Equal(_Capitalizacao, target.Capitalization);

        }

        /// <summary>
        ///A test for MOVIMENTO Constructor
        ///</summary>
        [Fact]
        public void MOVIMENTOConstructorTest()
        {
            Account _Contas = new Account();
            Decimal _Valor = 256.26m;
            DateTime _Dt_Movimento = new DateTime(2009, 02, 05);
            string _Descricao = "Descricao 1";
            bool _Creditado = false;
            bool _Capitalizacao = false;

            Transaction target = new Transaction(_Contas, _Valor, _Dt_Movimento, _Descricao, _Capitalizacao, _Creditado, null, AccountTest.UserId);

            Assert.Equal(_Contas, target.Account);
            Assert.Equal(_Valor, target.TransactionValue);
            Assert.Equal(_Dt_Movimento, target.TransactionDate);
            Assert.Equal(_Descricao, target.TransactionDescription);
            Assert.Equal(_Creditado, target.AccountTransfer);
            Assert.Equal(_Capitalizacao, target.Capitalization);

            _Creditado = true;
            _Capitalizacao = true;


            target = new Transaction(_Contas, _Valor, _Dt_Movimento, _Descricao, _Capitalizacao, _Creditado, null, AccountTest.UserId);

            Assert.Equal(_Contas, target.Account);
            Assert.Equal(_Valor, target.TransactionValue);
            Assert.Equal(_Dt_Movimento, target.TransactionDate);
            Assert.Equal(_Descricao, target.TransactionDescription);
            Assert.Equal(_Creditado, target.AccountTransfer);
            Assert.Equal(_Capitalizacao, target.Capitalization);


            // Assert.Inconclusive("TODO: Implement code to verify target");
        }



        [Fact]
        public void MOVIMENTOClassifica_Tipo_MovimentoTest()
        {            /*
            Account _Contas = new Account();
            Decimal _Valor = 256.26m;
            DateTime _Dt_Movimento = new DateTime(2009, 02, 05);
            string _Descricao = "Descricao 1";
            bool _Capitalizacao = false;
            TransactionType Tp_Movimento;

            Transaction target = new Transaction(_Contas, _Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false, Tp_Movimento, AccountTest.UserId);

            Tp_Movimento = target.Classifica_Tipo_Movimento(Db.IDENTIFICACAO_MOVIMENTO);
            Assert.IsNull(Tp_Movimento);

            _Descricao = "RSHOP-BRASILIA PO01/09";
            target.ChangeTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false);
            Tp_Movimento = target.Classifica_Tipo_Movimento(Db.IDENTIFICACAO_MOVIMENTO);
            Assert.Equal(2, Tp_Movimento.ID_TIPO_MOVIMENTO);

            _Descricao = "CH COMPENSADO26/85";
            _Valor = -400;
            target.ChangeTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false);
            Tp_Movimento = target.Classifica_Tipo_Movimento(Db.IDENTIFICACAO_MOVIMENTO);
            Assert.Equal(5, Tp_Movimento.ID_TIPO_MOVIMENTO);

            _Descricao = "CH COMPENSADO45/65";
            _Valor = 400.01m;
            target.ChangeTransaction(_Valor, _Dt_Movimento, _Descricao, _Capitalizacao, false);
            Tp_Movimento = target.Classifica_Tipo_Movimento(Db.IDENTIFICACAO_MOVIMENTO);
            Assert.IsNull(Tp_Movimento);


            ///////////////////////////////////////////////
            // Teste para classificar todos os movimentos//
            ///////////////////////////////////////////////

            List<MOVIMENTO> _Lista_Movimentos = Db.MOVIMENTO.Where(x => x.TIPO_MOVIMENTO == null && !x.TRANSFERENCIA_CONTA).ToList<MOVIMENTO>();
            //List<MOVIMENTO> _Lista_Movimentos = Db.MOVIMENTO.Where(x => x.TIPO_MOVIMENTO == null && x.DT_MOVIMENTO > new DateTime(2009, 07, 01)).ToList<MOVIMENTO>();
            foreach (MOVIMENTO m in _Lista_Movimentos)
            {
                Tp_Movimento = m.Classifica_Tipo_Movimento(Db.IDENTIFICACAO_MOVIMENTO);
                if (m.TIPO_MOVIMENTO != Tp_Movimento)
                {
                    m.TIPO_MOVIMENTO = Tp_Movimento;
                    Db.SaveChanges();
                }
            }

            */
        }
    }
}
