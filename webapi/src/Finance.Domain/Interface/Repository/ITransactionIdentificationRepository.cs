﻿using Finance.Domain.Interface.Common;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Interface.Repository
{
    public interface ITransactionIdentificationRepository : IRepository<TransactionIdentification>
    {
        Task<TransactionType> FindTransactionType(string description, decimal value);
    }
}
