﻿using Finance.Domain.Interface.Common;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Interface.Repository
{
    public interface IAccountRepository : IRepository<Account>
    {
        Task<ICollection<Account>> GetActiveAccounts(string userId);
        Task<ICollection<Account>> GetAllAccounts(string userId);
        Task<Account> GetUserAccount(string userId, int accountId);
    }
}
