﻿using Finance.Domain.Interface.Common;
using Finance.Domain.Dto;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Interface.Common
{
    public interface IUserService : IService<User>
    {
        Task<bool> ValidateLogin(string loginEmail, string password);
        Task<User> GetUser(string loginEmail);
        Task<CreateUserDto> CreateUser(CreateUserDto createUser);
        Task<bool> ChangeUser(CreateUserDto createUser);
        Task<bool> ChangePassword(ChangePasswordUser changePasswordUser);
    }
}
