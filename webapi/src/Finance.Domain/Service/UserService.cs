﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Interface.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Model;
using Finance.Domain.Dto;
using Finance.Domain.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Service
{
    public class UserService : ServiceBase<User, FinanceContext>, IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository, IUnitOfWork<FinanceContext> uow) : base(repository, uow)
        {
            _repository = repository;
        }
        
        public async Task<bool> ValidateLogin(string loginEmail, string password)
        {
            var login = await _repository.GetLogin(loginEmail);

            if (login == null)
                return false;
            
            var isAuthenticated = login.ValidatePassword(password);

            // update database trying wrong password
            await this.Commit();

            return isAuthenticated;
        }

        
        public async Task<User> GetUser(string loginEmail)
        {
            var login = await _repository.FindFirst(x => x.LoginEmail == loginEmail);
            return login;
        }

        
        public async Task<CreateUserDto> CreateUser(CreateUserDto createUser)
        {
            var newUser = await _repository.GetLogin(createUser.LoginEmail);

            if (newUser == null)
            {
                newUser = User.CreateUser(createUser);
                _repository.Add(newUser);
            }
            else
            {
                // usuário já existe, validar senha
                if (newUser.ValidatePassword(createUser.NewPassword))
                    newUser.ChangeUser(createUser.UserName);
                else
                {
                    // update database trying wrong password
                    await this.Commit();
                    return null;
                }
            }           

            // SaveUSerData
            await this.Commit();

            // consultar novamente para garantir que tudo foi gravado
            newUser = await _repository.GetLogin(createUser.LoginEmail);

            return newUser.ConvertToDto();

        }

        public async Task<bool> ChangeUser(CreateUserDto createUser)        
        {
            var newUser = await _repository.GetLogin(createUser.LoginEmail);

            if (newUser == null)
                return false;
            
            // usuário já existe, validar senha
            newUser.ChangeUser(createUser.UserName);                 

            // SaveUSerData
            await this.Commit();

            return true;            
        }

        public async Task<bool> ChangePassword(ChangePasswordUser changePasswordUser)        
        {
            var newUser = await _repository.GetId(changePasswordUser.UserID);

            if (newUser == null)
                return false;
            
            // usuário já existe, validar senha
            var passwordChanged = newUser.ChangePassword(changePasswordUser);                 

            // SaveUSerData
            await this.Commit();

            return passwordChanged;    
            
        }
    }
}
