﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Interface.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Model;
using Finance.Domain.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Service
{
    public class TransactionIdentificationService : ServiceBase<TransactionIdentification, FinanceContext>, ITransactionIdentificationService
    {
        private readonly ITransactionIdentificationRepository _repository;

        public TransactionIdentificationService(ITransactionIdentificationRepository repository, IUnitOfWork<FinanceContext> uow) : base(repository, uow)
        {
            _repository = repository;
        }

    }
}
