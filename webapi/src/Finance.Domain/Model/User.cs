using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Finance.Domain.Dto;

namespace Finance.Domain.Model
{

    public class User
    {
        private static readonly string PasswordSalt = "sdonfisufnliwsefnweinisvub";

        public string UserId { get; set; }
        public string LoginEmail { get; set; }
        public string PasswordHash { get; set; }
        public string UserName { get; set; }
        public int WrongPasswordAttempts { get; set; }
        public bool Blocked { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public bool ValidatePassword(string password)
        {
            if (this.Blocked)
                return false;


            var hashedPassword = HashPassword(password);
            bool passwordMatch = this.PasswordHash == hashedPassword;

            if (!passwordMatch)
            {
                this.WrongPasswordAttempts++;

                if (this.WrongPasswordAttempts > 3)
                {
                    this.Blocked = true;
                }
            }
            else
            {
                this.WrongPasswordAttempts = 0;
                this.LastLoginDate = DateTime.Now;
            }

            return passwordMatch;
        }

        private static string HashPassword(string password)
        {
            // SHA256 is disposable by inheritance.  
            using(var sha256 = SHA256.Create())  
            {  
                // Send a sample text to hash.  
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password + PasswordSalt));  
                // Get the hashed string.  
                var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();  
                
                return hash;
            } 
        }

        public static User CreateUser(CreateUserDto createUser)
        {
            var hashedPassword = HashPassword(createUser.NewPassword);

            var newUser = new User()
            {
                UserId = Guid.NewGuid().ToString(),
                LoginEmail = createUser.LoginEmail,
                PasswordHash = hashedPassword,
                UserName = createUser.UserName,
                WrongPasswordAttempts = 0,
                Blocked = false,
                CreateDate = DateTime.Now,
                LastLoginDate = DateTime.Now
            };

            return newUser;
        }

        public void ChangeUser(string userName)
        {
            this.UserName = userName;
        }

        public bool ChangePassword(ChangePasswordUser changePasswordUser)
        {
            if (this.ValidatePassword(changePasswordUser.OldPassword))
            {
                var hashedPassword = HashPassword(changePasswordUser.NewPassword);
                this.PasswordHash = hashedPassword;
                return true;
            }
            else
                return false;
        }

        public CreateUserDto ConvertToDto()
        {
            return new CreateUserDto()
            {
                UserID = this.UserId,
                LoginEmail = this.LoginEmail,
                UserName = this.UserName
            };
        }

    }
}
