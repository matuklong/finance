using System;
using System.Collections.Generic;
using System.Linq;

namespace Finance.Domain.Dto
{
    public class CreateUserDto
    {
        public string UserID { get; set; }
        public string LoginEmail { get; set; }
        public string NewPassword { get; set; }
        public string UserName { get; set; }
    }
}