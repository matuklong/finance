using System;
using System.Collections.Generic;
using System.Linq;

namespace Finance.Domain.Dto
{
    public class ChangePasswordUser
    {
        public string UserID { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}