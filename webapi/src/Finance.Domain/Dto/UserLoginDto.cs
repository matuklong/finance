namespace Finance.Domain.Dto
{
    public class UserLoginDto  
    {
        public string loginEmail { get; set; }
        public string password { get; set; }
    }
}