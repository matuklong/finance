﻿using Finance.Domain.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Context
{
    public class DbContextBase : DbContext, IDbContextBase
    {
        public DbContextBase()
      : base()
        { }

        public DbContextBase(DbContextOptions options)
      : base(options)
        { }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public int? CurrentUserId { get; private set; }
    }
}
