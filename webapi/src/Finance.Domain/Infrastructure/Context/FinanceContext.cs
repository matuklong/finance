﻿using Finance.Domain.Infrastructure.Mappings;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Finance.Domain.Infrastructure.Context
{
    public class FinanceContext : DbContextBase
    {
        public FinanceContext()
        {
            Database.SetCommandTimeout(120000);
        }

        public FinanceContext(DbContextOptions options) : base(options)
        {
            Database.SetCommandTimeout(120000);
        }

        private void ModelBuilderAddConfigurations(ModelBuilder modelBuilder)
        {
            AccountMap.Map(modelBuilder);
            TransactionMap.Map(modelBuilder);
            TransactionIdentificationMap.Map(modelBuilder);
            TransactionTypeMap.Map(modelBuilder);
            UserMap.Map(modelBuilder);
        }
        
        #region DBSets

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionIdentification> TransactionIdentifications { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<User> User { get; set; }
        
        #endregion
    }
}
