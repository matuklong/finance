﻿using Finance.Domain.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Context
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext>, IDisposable where TContext : IDbContextBase, new()
    {
        //ServiceLocator.Current.GetInstance<IContextManager<TContext>>();// as ContextManager<TContext>;
        private readonly IDbContextBase _dbContext;
        private bool _disposed;

        public UnitOfWork(IDbContextBase dbContext)
        {
            _dbContext = dbContext;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void BeginTransaction()
        {
            _disposed = false;
        }

        public async Task Commit()
        {
            await _dbContext.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            _disposed = true;
        }
    }

}
