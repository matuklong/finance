﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Infrastructure.Repository.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDbContextBase context):base(context)
        {

        }
        public async Task<User> GetLogin(string loginEmail)
        {
            var db = (FinanceContext)Context;
            return await db.User.Where(x => x.LoginEmail == loginEmail).FirstOrDefaultAsync();
        }
        public async Task<User> GetId(string userId)
        {
            var db = (FinanceContext)Context;
            return await db.User.Where(x => x.UserId == userId).FirstOrDefaultAsync();
        }
    }
}
