﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Infrastructure.Repository.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Repository
{
    public class TransactionIdentificationRepository : RepositoryBase<TransactionIdentification>, ITransactionIdentificationRepository
    {
        public TransactionIdentificationRepository(IDbContextBase context):base(context)
        {

        }

        public async Task<TransactionType> FindTransactionType(string description, decimal value)
        {
            var db = (FinanceContext)Context;            
            return await (from o in db.TransactionIdentifications
                       where(
                                    (o.Description.Length > 1 && description.Trim().Contains(o.Description.Trim()))
                                    || (o.Description.Length == 1 && description.Trim() == o.Description.Trim())
                                    || o.Description == null
                             )
                             && (value == o.TransactionValue || o.TransactionValue == null)
                    select o.TransactionType).FirstOrDefaultAsync();
    }

    }
}
