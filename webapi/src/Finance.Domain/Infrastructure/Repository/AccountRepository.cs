﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Infrastructure.Repository.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Repository
{
    public class AccountRepository : RepositoryBase<Account>, IAccountRepository
    {
        public AccountRepository(IDbContextBase context):base(context)
        {

        }

        public async Task<ICollection<Account>> GetActiveAccounts(string userId)
        {
            var db = (FinanceContext)Context;
            return await db.Accounts.Where(x => x.UserId == userId && x.Active).ToListAsync();
        }

        public async Task<ICollection<Account>> GetAllAccounts(string userId)
        {
            var db = (FinanceContext)Context;
            return await db.Accounts.Where(x => x.UserId == userId).ToListAsync();
        }

        public async Task<Account> GetUserAccount(string userId, int accountId)
        {
            var db = (FinanceContext)Context;
            return await db.Accounts.Where(x => x.UserId == userId && x.AccountId == accountId).FirstOrDefaultAsync();
        }
    }
}
