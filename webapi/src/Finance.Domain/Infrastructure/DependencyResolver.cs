﻿using Finance.Domain.Infrastructure.Context;
using Finance.Domain.Infrastructure.Interfaces;
using Finance.Domain.Infrastructure.Repository;
using Finance.Domain.Infrastructure.Repository.Common;
using Finance.Domain.Interface.Common;
using Finance.Domain.Interface.Repository;
using Finance.Domain.Service;
using Finance.Domain.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Finance.Domain.Infrastructure.DependencyResolver
{
    public static class FinanceDependencyResolver
    {
        
        public static void Resolve(IServiceCollection services, string connectionString)
        {
            
            services.AddScoped<IDbContextBase>((provider) => {
                var options = new DbContextOptionsBuilder<DbContextBase>();
                options.UseSqlServer(connectionString);
                return new FinanceContext(options.Options);
            });
            services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));

            services.AddScoped(typeof(IRepository<>), typeof(RepositoryBase<>));

            //RegisterApplicationResolver(services);
            RegisterServicesResolver(services);
            RegisterRepositoryResolver(services);
        }


        #region Services

        private static void RegisterServicesResolver(IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<ITransactionTypeService, TransactionTypeService>();
            services.AddScoped<ITransactionIdentificationService, TransactionIdentificationService>();
            services.AddScoped<IUserService, UserService>();
        }

        #endregion

        #region Repositories

        private static void RegisterRepositoryResolver(IServiceCollection services)
        {
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            //services.AddScoped<ITransactionTypeRepository, TransactionTypeRepository>();
            services.AddScoped<ITransactionIdentificationRepository, TransactionIdentificationRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }

        #endregion
        
    }
}
