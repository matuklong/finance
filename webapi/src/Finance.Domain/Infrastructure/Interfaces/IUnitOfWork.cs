﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Interfaces
{
    public interface IUnitOfWork<TContext> : IDisposable where TContext : IDbContextBase, new()
    {
        void BeginTransaction();
        Task Commit();
    }
}
