﻿using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Mappings
{
    public static class AccountMap
    {
        public static void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(m =>
            {
                m.ToTable("Account");
                m.HasKey(t => t.AccountId);
                m.Property(a => a.UserId)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(a => a.BankName)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(a => a.AccountAgency)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(a => a.AccountNumber)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(a => a.BalanceValue)
                    .IsRequired();
                m.Property(a => a.AccountDescription)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(a => a.LastTransactionDate);
                m.Property(a => a.Active)
                    .IsRequired();
            });
        }
    }
}
