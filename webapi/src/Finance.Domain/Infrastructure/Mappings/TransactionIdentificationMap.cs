﻿using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Mappings
{
    public static class TransactionIdentificationMap
    {
        public static void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionIdentification>(m =>
            {
                m.ToTable("TransactionIdentification");
                m.HasKey(t => t.TransactionIdentificationId);
                m.Property(c => c.UserId)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(c => c.Description)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(c => c.Description)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(c => c.TransactionValue);
                m.HasOne(t => t.TransactionType)
                    .WithMany(tt => tt.TransactionIdentification)
                    .HasForeignKey(t => t.TransactionTypeId);
            });
        }
    }
}
