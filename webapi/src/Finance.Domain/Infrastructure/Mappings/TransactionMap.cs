﻿using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Mappings
{
    public static class TransactionMap
    {
        public static void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>(m =>
            {
                m.ToTable("Transaction");
                m.HasKey(t => t.TransactionId);
                m.Property(t => t.UserId)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(t => t.TransactionValue)
                    .IsRequired();
                m.Property(t => t.InclusionDate)
                    .IsRequired();
                m.Property(t => t.TransactionDate)
                    .IsRequired();
                m.Property(t => t.TransactionDescription)
                    .IsRequired()
                    .HasMaxLength(50);
                m.Property(t => t.Capitalization)
                    .IsRequired();
                m.Property(t => t.BalanceBeforeTransaction)
                    .IsRequired();
                m.Property(t => t.AccountTransfer)
                    .IsRequired();
                m.HasOne(t => t.TransactionType)
                    .WithMany(tt => tt.Transaction)
                    .HasForeignKey(t => t.TransactionTypeId);
                m.HasOne(t => t.Account)
                    .WithMany(tt => tt.Transaction)
                    .HasForeignKey(t => t.AccountId)
                    .IsRequired();
            });
        }
    }
}