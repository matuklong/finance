﻿using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Mappings
{
    public static class UserMap
    {
        public static void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(m =>
            {
                m.ToTable("User");

                m.HasKey(t => t.UserId);

                m.Property(c => c.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                m.Property(c => c.LoginEmail)
                    .IsRequired()
                    .HasMaxLength(200); 

                m.Property(c => c.UserName)
                    .IsRequired()
                    .HasMaxLength(200); 

                m.Property(c => c.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(200);           
            });

        }
    }
}
