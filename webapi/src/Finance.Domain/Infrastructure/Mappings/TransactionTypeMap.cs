﻿using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Domain.Infrastructure.Mappings
{
    public static class TransactionTypeMap
    {
        public static void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionType>(m =>
            {
                m.ToTable("TransactionType");

                m.HasKey(t => t.TransactionTypeId);

                m.Property(c => c.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                m.Property(c => c.Description)
                    .IsRequired()
                    .HasMaxLength(50);           
            });

        }
    }
}
