﻿using Finance.Domain.Interface.Common;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Finance.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BankAccountController : FinanceController
    {
        private IAccountService _accountService;
        

        public BankAccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        // GET: api/BanckAccount
        [HttpGet]
        public async Task<IEnumerable<Account>> Get()
        {
            return await _accountService.GetAllAccounts(userId);
        }

        // GET: api/BanckAccount/5        
        [HttpGet("{id}")]
        public async Task<Account> Get(int id)
        {
            return await _accountService.GetAccount(userId, id);
        }

        // POST: api/BanckAccount
        [HttpPost]
        public async Task<object> Post([FromBody]Account account)
        {
            await _accountService.AddAccount(account.BankName, account.AccountAgency, account.AccountNumber, account.AccountDescription, userId);
            return new { Message = "Conta cadastrada com sucesso", Error = false };
        }

        // PUT: api/BanckAccount
        [HttpPut]
        public async Task<object> Put([FromBody]Account account)
        {
            await _accountService.ChangeAccount(account.AccountId, account.BankName, account.AccountAgency, account.AccountNumber, account.AccountDescription, userId);
            return new { Message = "Conta atualizada com sucesso", Error = false };
        }

    }
}
