using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Finance.WebApi.Controllers
{
    [Authorize]
    public class FinanceController: Controller
    {
        protected string userId {get {return this.GetUserId();}}

        public string GetUserId()
        {
            if (HttpContext == null || HttpContext.User == null)
                return null;

            var currentUser = HttpContext.User;
            var tempUserId = currentUser.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            return tempUserId;
        }
    }
}
