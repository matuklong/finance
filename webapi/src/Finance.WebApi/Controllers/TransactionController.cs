﻿using Finance.Domain.Interface.Common;
using Finance.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Finance.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : FinanceController
    {
        // get the latest 2 months
        private DateTime BaseDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(-2);

        ITransactionService _transactionService;
        IAccountService _accountService;
        public TransactionController(ITransactionService transactionService, IAccountService accountService)
        {
            _transactionService = transactionService;
            _accountService = accountService;
        }

        // GET: api/Transaction
        [HttpGet]
        public async Task<IEnumerable<Transaction>> Get()
        {
            return await _transactionService.GetTransactionFromDate(userId, this.BaseDate);
        }

        // GET: api/Transaction/5
        [HttpGet("{id}")]
        public async Task<IEnumerable<Transaction>> Get(int id)
        {
            return await _transactionService.GetTransactionFromAccount(userId, id, this.BaseDate);
        }

        // POST: api/Transaction
        [HttpPost]
        public async Task<object> Post([FromBody]Transaction transaction)
        {
            await _transactionService.AddTransaction(transaction.AccountId, transaction.TransactionValue, transaction.TransactionDate, transaction.TransactionDescription, transaction.Capitalization, transaction.AccountTransfer, eTransactionType.TransactionValue, userId);
            return new { Message = "Transação cadastrada com sucesso", Error = false };
        }

        // PUT: api/Transaction/5
        [HttpPut]
        public async Task<object> Put([FromBody]Transaction transaction)
        {
            await _transactionService.ChangeTransaction(transaction.AccountId, transaction.TransactionId, transaction.TransactionValue, transaction.TransactionDate, transaction.TransactionDescription, transaction.Capitalization, transaction.AccountTransfer, eTransactionType.TransactionValue, userId);
            return new { Message = "Transação atualizada com sucesso", Error = false };
        }

        // DELETE: api/Transaction/5
        [HttpDelete("{id}")]
        public async Task<object> Delete(int id)
        {
            await _transactionService.DeleteTransaction(id, userId);
            return new { Message = "Transação removida com sucesso", Error = false };
        }

        // GET: api/Transaction/GetSummary
        [HttpGet]
        [Route("GetSummary")]
        public async Task<IEnumerable<object>> GetSummary()
        {
            return await _transactionService.GetTransactionSummaryByMonth(userId, this.BaseDate);
        }
    }
}
