﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Finance.WebApi.Configuration;
using Finance.Domain.Interface.Common;
using Finance.Domain.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.IdentityModel.Tokens;
using Finance.Domain.Dto;

namespace Finance.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : FinanceController
    {
        [AllowAnonymous]
        [HttpPost]
        public async Task<object> Post(
            [FromBody] UserLoginDto usuario,
            [FromServices]IUserService usersService,
            [FromServices]SigningConfigurations signingConfigurations,
            [FromServices]TokenConfigurations tokenConfigurations)
        {
            bool credenciaisValidas = false;
            if (usuario != null 
                && !String.IsNullOrWhiteSpace(usuario.loginEmail)
                && !String.IsNullOrWhiteSpace(usuario.password)
                )
            {
                credenciaisValidas = await usersService.ValidateLogin(usuario.loginEmail, usuario.password);
            }
            
            if (credenciaisValidas)
            {
                var userData = await usersService.GetUser(usuario.loginEmail);

                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(userData.LoginEmail, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, userData.UserId),
                        new Claim(JwtRegisteredClaimNames.GivenName, userData.UserName)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new
                {
                    authenticated = true,
                    created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    accessToken = token,
                    message = "OK",
                    userId = userData.UserId,
                    userName = userData.UserName
                };
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                };
            }
        }
        
        [AllowAnonymous]
        [HttpPost("CreateUser")]
        public async Task<object> CreateUser(
            [FromBody] CreateUserDto usuario,
            [FromServices]IUserService usersService)
        {
            var newUser = await usersService.CreateUser(usuario);
            return newUser;
        }
        
        [HttpPost("ChangeUser")]
        public async Task<object> ChangeUser(
            [FromBody] CreateUserDto usuario,
            [FromServices]IUserService usersService)
        {
            usuario.UserID = this.GetUserId();
            var newUser = await usersService.ChangeUser(usuario);
            return newUser;
        }
        
        [HttpPost("ChangeUser")]
        public async Task<object> ChangeUser(
            [FromBody] ChangePasswordUser usuario,
            [FromServices]IUserService usersService)
        {
            usuario.UserID = this.GetUserId();
            var newUser = await usersService.ChangePassword(usuario);
            return newUser;
        }

    }
}
