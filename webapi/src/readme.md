Comandos executados para o dotnet core 2

dotnet new solution -n Finance.WebApi
dotnet new classlib --name Finance.Domain
dotnet sln Finance.WebApi.sln add .\Finance.Domain\Finance.Domain.csproj
dotnet new web --name Finance.WebApi
dotnet sln Finance.WebApi.sln add .\Finance.WebApi\Finance.WebApi.csproj
cd Finance.WebApi
dotnet add reference ..\Finance.Domain\Finance.Domain.csproj
cd ..
dotnet new xunit --name Finance.Test
dotnet sln Finance.WebApi.sln add .\Finance.Test\Finance.Test.csproj
cd Finance.Test
dotnet add reference ..\Finance.Domain\Finance.Domain.csproj
cd ..


dotnet ef migrations add InitialMigration -p ..\Finance.Domain\Finance.Domain.csproj -s Finance.WebApi.csproj -c FinanceContext
dotnet ef database update