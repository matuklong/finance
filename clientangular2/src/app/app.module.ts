import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { HttpClientModule } from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';

// fusion charts
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import { FusionChartsModule } from 'angular4-fusioncharts';

// currency format
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';

// ngx-bootstrap
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

// route config
import { routes } from './finance.route';

// services
import { Globals } from './globals';
import { FinanceService } from './finance.service';

// components
import { AccountListComponent } from './account-list/account-list.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { TransactionComponent } from './transaction/transaction.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { AccountAddUpdateComponent } from './account-add-update/account-add-update.component';
import { AuthGuard } from './finance.auth.guard';
import { AuthenticationService } from './finance.auth.service';
import { UserService } from './finance.user.services';
import { LoginComponent } from './login/login.component';
import { UserinfoComponent } from './userinfo/userinfo.component';
import { CreateUserComponent } from './create-user/create-user.component';


// Currency default format
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.'
};

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);
registerLocaleData(localePt);

@NgModule({
  declarations: [
    AccountListComponent,
    HomeComponent,
    MenuComponent,
    TransactionComponent,
    DashboardComponent,
    AccountComponent,
    AccountAddUpdateComponent,
    LoginComponent,
    UserinfoComponent,
    CreateUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    CurrencyMaskModule,

    // ngx-bootstrap
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),

    // fusioncharts
    FusionChartsModule,
  ],
  providers: [
    FinanceService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    AuthGuard,
    AuthenticationService,
    UserService,
    Globals,
  ],
  bootstrap: [HomeComponent]
})
export class AppModule { }
