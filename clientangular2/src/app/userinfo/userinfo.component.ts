import { Component, OnInit, OnDestroy } from '@angular/core';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../finance.auth.service';
import { User } from '../finance.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-userinfo',
  templateUrl: './userinfo.component.html',
  styles: [`
  #logoutButton, #createUserButton {
    margin-left: 15px;
    margin-right: 15px;
  }
  `]
})
export class UserinfoComponent implements OnInit, OnDestroy {

  user: User;
  private userSubscription: Subscription;

  constructor(private authenticationService: AuthenticationService) {
    this.user = new User;
  }

  ngOnInit() {
    this.userSubscription = this.authenticationService.user.subscribe((currentUser) => {
      this.user = currentUser;
      if (this.user == null) {
        this.user = new User();
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  doLogout() {
    this.authenticationService.logout();
  }
}
