import {Routes, RouterModule} from '@angular/router';
import { AccountListComponent } from './account-list/account-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { AccountAddUpdateComponent } from './account-add-update/account-add-update.component';
import { AuthGuard } from './finance.auth.guard';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './create-user/create-user.component';

export const routes: Routes = [
    { path: 'transactions', component: AccountListComponent, canActivate: [AuthGuard] },
    { path: 'account/:accountId', pathMatch: 'full', component: AccountAddUpdateComponent, canActivate: [AuthGuard] },
    { path: 'account', component: AccountComponent, canActivate: [AuthGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'createuser', component: CreateUserComponent },
    { path: '', pathMatch: 'full', redirectTo: 'dashboard' }
   ];
