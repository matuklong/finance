import 'rxjs/add/operator/switchMap';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Location } from '@angular/common';
import { Account } from '../finance.model';
import { FinanceService } from '../finance.service';


@Component({
  selector: 'app-account-add-update',
  templateUrl: './account-add-update.component.html',
  styles: []
})

export class AccountAddUpdateComponent implements OnInit {

    account: Account;

    constructor(
      private financeService: FinanceService,
      private route: ActivatedRoute,
      private location: Location
    ) { }

    ngOnInit(): void {
      this.route.params.switchMap((params: Params) => {
        if (isNaN(Number(params.accountId))) {
          const acc = new Account();
          acc.accountId = 0;
          return [acc];
        } else {
          return this.financeService.getAccountById(+params.accountId);
        }
      })
      .subscribe(account => {
        this.account = account;
      });
    }

    updateAccount(): void {
      this.financeService.AddUpdateAccount(this.account);
      this.goBack();
    }

    goBack(): void {
      this.location.back();
    }

  }

