import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Account, DataReturn, Transaction } from './finance.model';
import { AuthenticationService } from './finance.auth.service';
import { Globals } from './globals';

@Injectable()
export class FinanceService {

  // async data store: https://coryrylan.com/blog/angular-observable-data-services
  AccountList: Observable<Account[]>;
  private _AccountList: BehaviorSubject<Account[]>;


  TransactionList: Observable<Transaction[]>;
  private _TransactionList: BehaviorSubject<Transaction[]>;

  private dataStore: {
    accountList: Account[],
    transactionList: Transaction[]
  };

  constructor(
    private http: HttpClient,
    private globals: Globals,
    private authenticationService: AuthenticationService
  ) {
    this.dataStore = { accountList: [], transactionList: []};
    this._AccountList = <BehaviorSubject<Account[]>>new BehaviorSubject([]);
    this.AccountList = this._AccountList.asObservable();


    this._TransactionList = <BehaviorSubject<Transaction[]>>new BehaviorSubject([]);
    this.TransactionList = this._TransactionList.asObservable();
  }

  getAccounts() {
    if (this.dataStore.accountList.length === 0) {
      this.http
          .get<Account[]>(this.globals.UrlServiceV1 + 'BankAccount', { headers: this.authenticationService.getHeaders() })
          .subscribe(data => {
            this.dataStore.accountList = data;
            this._AccountList.next(Object.assign({}, this.dataStore).accountList);
            }, error => this.handleError(error)
          );
    }
  }

  getAccountById(accountId: Number): Observable<Account> {
    return this.http
        .get<Account>(this.globals.UrlServiceV1 + 'BankAccount/' + accountId, { headers: this.authenticationService.getHeaders() });
  }

  getTransactions(accountId: number) {
    // return this.http
    //     .get<Transaction[]>(this.globals.UrlServiceV1 + "Transaction/" + accountId);
    if (this.dataStore.transactionList.length === 0 || this.dataStore.transactionList[0].accountId !== accountId) {
      this.http
          .get<Transaction[]>(this.globals.UrlServiceV1 + 'Transaction/' + accountId, { headers: this.authenticationService.getHeaders() })
          .subscribe(data => {
            this.dataStore.transactionList = data;
            this._TransactionList.next(Object.assign({}, this.dataStore).transactionList);
            }, error => this.handleError(error));
    }
  }

  AddUpdateAccount(account: Account) {

    if (account.accountId === undefined || account.accountId === 0) {
      this.http.post(this.globals.UrlServiceV1 + 'BankAccount', account, { headers: this.authenticationService.getHeaders() })
      .subscribe(data => {
          this.dataStore.accountList.push(account);
          this._AccountList.next(Object.assign({}, this.dataStore).accountList);
      }, error => this.handleError(error));
    } else {
      this.http.put(this.globals.UrlServiceV1 + 'BankAccount', account, { headers: this.authenticationService.getHeaders() })
      .subscribe(data => {
        this.dataStore.accountList.forEach((t, i) => {
          if (t.accountId === account.accountId) { this.dataStore.accountList[i] = account; }
        });

        this._AccountList.next(Object.assign({}, this.dataStore).accountList);
      }, error => this.handleError(error));
    }
  }

  AddUpdateTransaction(transaction: Transaction) {
    if (transaction.transactionId === undefined || transaction.transactionId === 0) {
      this.http.post(this.globals.UrlServiceV1 + 'Transaction', transaction, { headers: this.authenticationService.getHeaders() })
      .subscribe(data => {
          this.dataStore.transactionList.push(transaction);
          this._TransactionList.next(Object.assign({}, this.dataStore).transactionList);
      }, error => this.handleError(error));
    } else {
      this.http.put(this.globals.UrlServiceV1 + 'Transaction', transaction, { headers: this.authenticationService.getHeaders() })
      .subscribe(data => {
        this.dataStore.transactionList.forEach((t, i) => {
          if (t.transactionId === transaction.transactionId) { this.dataStore.transactionList[i] = transaction; }
        });

        this._TransactionList.next(Object.assign({}, this.dataStore).transactionList);
      }, error => this.handleError(error));
    }

  }

  DeleteTransaction(transaction: Transaction) {
    if (transaction.transactionId === undefined || transaction.transactionId === 0) {
      return false;
    } else {
      this.http.delete(this.globals.UrlServiceV1 + 'Transaction/' + transaction.transactionId,
        { headers: this.authenticationService.getHeaders() })
      .subscribe(data => {
        this.dataStore.transactionList.forEach((t, i) => {
          if (t.transactionId === transaction.transactionId) { this.dataStore.transactionList.splice(i, 1); }
        });

        this._TransactionList.next(Object.assign({}, this.dataStore).transactionList);
      }, error => this.handleError(error));
    }

  }

  private handleError(error: any): Promise<any> {
    if (error.stauts = 401) {
      console.error('Unauthorized');
      this.authenticationService.logout();
      return Promise.reject(error.message || error);
    } else {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }
  }
}
