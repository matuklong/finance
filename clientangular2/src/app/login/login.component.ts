import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../finance.auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [`
    .row {
      margin-top: 15px;
      margin-bottom: 15px;
    }
  `]
})
export class LoginComponent implements OnInit {
  userEmail: string;
  userPassword: string;
  error = '';
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  doLogin() {
    // this.loading = true;
    this.authenticationService.login(this.userEmail, this.userPassword)
      .subscribe(result => {
        if (result === true) {
            // login successful
            this.router.navigate(['/']);
        } else {
            // login failed
            this.error = 'Username or password is incorrect';
            // this.loading = false;
        }
    });
  }

}
