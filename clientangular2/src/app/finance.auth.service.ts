import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import { Globals } from './globals';
import { User } from './finance.model';

// http://jasonwatmore.com/post/2016/08/16/angular-2-jwt-authentication-example-tutorial

@Injectable()
export class AuthenticationService {
    public token: string;
    public user: Observable<User>;
    public _user: BehaviorSubject<User>;
    private currentUser: User;

    constructor(
        private http: HttpClient,
        private globals: Globals) {

        // Observables
        this._user = <BehaviorSubject<User>>new BehaviorSubject(null);
        this.user = this._user.asObservable();

        // Chame este método para forçar a atualizar os componentes
        this.changeCurrentUser();
        // set token if saved in local storage
        this.token = this.currentUser && this.currentUser.token;
    }

    public getCurrentUser() {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return currentUser;
    }

    private changeCurrentUser() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this._user.next(this.currentUser);
    }

    public getHeaders() {
        if (!this.token) {
            const currentUser = this.getCurrentUser();
            this.token = currentUser && currentUser.token;
        }

        let headers = new HttpHeaders();
        headers = headers.set('Authorization', 'Bearer ' + this.token);

        return headers;
    }

    public login(loginEmail: string, password: string): Observable<boolean> {
        const postLogin = { loginEmail: loginEmail, password: password };
        return this.http.post<any>(this.globals.UrlServiceV1 + 'login', postLogin)
            .map(response => {
                // login successful if there's a jwt token in the response
                const token = response !== undefined && response.accessToken;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({
                        loginEmail: loginEmail,
                        token: token,
                        userId: response.userId,
                        userName: response.userName
                    }));

                    this.changeCurrentUser();

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    public logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.currentUser = null;
        localStorage.removeItem('currentUser');
        this._user.next(null);
    }

    public createUser(user: User): Observable<User> {
        const postCreateUser = { LoginEmail: user.loginEmail, NewPassword: user.password, UserName: user.userName };
        return this.http.post<User>(this.globals.UrlServiceV1 + 'Login/CreateUser', postCreateUser);
    }

}
