import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FinanceService } from '../finance.service';
import { Account } from '../finance.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styles: [`
    .btnspace { margin-right: 5px; }
    `
  ]
})

export class AccountComponent implements OnInit {
    errorMessage: string;

    constructor(private router: Router, public financeService: FinanceService) {  }

    ngOnInit() {
      this.financeService.getAccounts();
    }

    private loadAccounts() {
      // this.financeService.getAccounts()
      //   .subscribe(
      //     accounts => {
      //       this.accountList = [];
      //       for (let acc of accounts) {
      //         this.accountList.push(acc);
      //       };
      //       //console.log(this.selectedAccount);
      //   },
      //   error => {
      //     this.errorMessage = error,
      //     console.log(error)
      //   });

    }

    accountEdit(event: any, account: Account): void {
      this.router.navigate(['/account/', account.accountId]);
    }

    accountAdd(event: any): void {
      this.router.navigate(['/account/new']);
    }

}
