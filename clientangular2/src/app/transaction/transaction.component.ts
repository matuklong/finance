import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FinanceService } from '../finance.service';
import { Transaction, Account } from '../finance.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styles: [`
    .btnspace { margin-right: 5px; }
    `
  ]
})
export class TransactionComponent implements OnInit {
  private _accountId = new BehaviorSubject<number>(0);
  private currentAccountId = 0;

  public editOrAdd = false;
  private transactionEdit: Transaction = null;

  @Input()
  set accountId(value: number) {
    this._accountId.next(value);
  }
  get accountId() {
    return this._accountId.getValue();
  }

  // transactionList: Transaction[];
  errorMessage: string;

  constructor(private financeService: FinanceService) { }

  ngOnInit() {
    // this.getTransactions();
    this._accountId
      .subscribe(x => {
        // console.log("new account event: " + x);
        if (x > 0) {
          this.currentAccountId = x;
          this.financeService.getTransactions(this.currentAccountId);
        }
      });
  }

  private getTransactions(accountIdParameter) {
    // this.transactionList = [];
    // this.financeService.getTransactions(accountIdParameter)
    //   .subscribe(
    //     transactions => {
    //       this.transactionList = [];
    //       for (let tran of transactions) {
    //         this.transactionList.push(tran);
    //         //console.log(acc);
    //       };
    //       //console.log(this.transactionList);
    //   },
    //   error => {
    //     this.errorMessage = error,
    //     console.log(error)
    //   });
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   const _accountId: SimpleChange = changes.accountId;
  //   console.log('prev value: ', _accountId.previousValue);
  //   console.log('got name: ', _accountId.currentValue);
  //   if (_accountId.previousValue !== _accountId.currentValue && _accountId.currentValue > 0){
  //     this.accountId = _accountId.currentValue;
  //     this.getTransactions();
  //   }
  // }

  onTransactionEdit(event: any, transaction: Transaction): void {
    this.editOrAdd = true;
    this.transactionEdit = transaction;
    // console.log(transaction);
    // this.router.navigate(['/account/', account.accountId]);
  }

  onTransactionRemove(event: any, transaction: Transaction): void {
    // this.router.navigate(['/account/', account.accountId]);
    this.financeService.DeleteTransaction(transaction);
  }

  onTransactionAdd(event: any): void {
    this.editOrAdd = true;
    this.transactionEdit = new Transaction();
    this.transactionEdit.transactionId = 0;
    this.transactionEdit.accountId = this.currentAccountId;
    // this.router.navigate(['/account/new']);
  }

  onConfirmEditOrAdd(event: any) {
    this.transactionEdit.userId = '00000000-0000-0000-0000-000000000000';
    this.transactionEdit.inclusionDate = new Date();
    this.financeService.AddUpdateTransaction(this.transactionEdit);

    this.editOrAdd = false;
    this.transactionEdit = null;
  }

  onCancelEditOrAdd(event: any) {
    this.editOrAdd = false;
    this.transactionEdit = null;
  }

}
