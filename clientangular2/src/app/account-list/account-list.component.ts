import { Component, OnInit } from '@angular/core';
import { FinanceService } from '../finance.service';
import { Account } from '../finance.model';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styles: [`
  .row {
    margin-top: 15px;
    margin-bottom: 15px;
  }
  `]
})
export class AccountListComponent implements OnInit {
  selectedAccount: Account;
  errorMessage: string;

  constructor(public financeService: FinanceService) {  }

  ngOnInit() {
    this.selectedAccount = null;
    // this.financeService.getAccounts()
    //   .subscribe(
    //     accounts => {
    //       this.accountList = [];
    //       for (let acc of accounts) {
    //         this.selectedAccount = this.selectedAccount || acc;
    //         this.accountList.push(acc);
    //         //console.log(acc);
    //       };
    //       //console.log(this.selectedAccount);
    //   },
    //   error => {
    //     this.errorMessage = error,
    //     console.log(error)
    //   });
    this.financeService.getAccounts();
  }

  public onAccountClick(event, account) {
    // console.log(account.accountId);
    this.selectedAccount = account;
  }

}

