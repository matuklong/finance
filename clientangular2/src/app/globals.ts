import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    readonly UrlServiceV1: string = 'http://localhost:5000/api/';
}
