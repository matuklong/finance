import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../finance.model';
import { AuthenticationService } from '../finance.auth.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { of } from 'rxjs/observable/of';
import { Location } from '@angular/common';
import { FinanceService } from '../finance.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styles: [`
    .row {
      margin-top: 15px;
      margin-bottom: 15px;
    }
  `]
})
export class CreateUserComponent implements OnInit, OnDestroy {

  user: User;
  createUserSuccess = null;
  private userSubscription: Subscription;

  constructor(
    private financeService: FinanceService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private location: Location) {
    this.createUserSuccess = null;
    this.user = new User;
  }

  ngOnInit() {
    this.userSubscription = this.authenticationService.user.subscribe((currentUser) => {
      this.user = currentUser;
      if (this.user == null) {
        this.user = new User();
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }


  createUserClick(): void {
    // implementar chamada do Criar usuário no authenticationService

    const currentEmail = this.user.loginEmail;
    const currentPassword = this.user.password;
    this.authenticationService.createUser(this.user).subscribe(data => {
      this.user = data;
      if (data.userId !== '') {
        this.authenticationService.login(currentEmail, currentPassword).subscribe(success => {
          this.createUserSuccess = success;
          this.router.navigate(['/']);
        }, error => { this.createUserSuccess = false; });
      } else {
        this.createUserSuccess = false;
      }
    }, error => { this.createUserSuccess = false; });
  }

  goBack(): void {
    this.location.back();
  }
}
