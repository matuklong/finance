import { Component, OnInit } from '@angular/core';
import { FinanceService } from '../finance.service';
import { Account } from '../finance.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  id = 'chart1';
  width = 600;
  height = 400;
  type = 'column2d';
  dataFormat = 'json';
  dataSource;


   constructor(public financeService: FinanceService) {

    this.dataSource = {
      'chart': {
          'caption': 'Saldo por Conta',
          'subCaption': '',
          'numberprefix': 'R$ ',
          'theme': 'fint'
      },
      'data': [
      ]
    };

  }

  ngOnInit() {
     this.financeService.getAccounts();

     this.financeService.AccountList.subscribe(accountList => {
       this.dataSource.data = accountList.map(data => {
        return { label: data.accountDescription, value: data.balanceValue};
      });
     });
  }

}
