export class DataReturn<T> {
    status: string;
    data: T;
}

export class Account {
    accountId: number;
    userId: string;
    bankName: string;
    accountAgency: string;
    accountNumber: string;
    balanceValue: number;
    accountDescription: string;
    lastTransactionDate?: Date;
    active: boolean;
}

export class Transaction {
    transactionId: number;
    userId: string;
    accountId: number;
    transactionTypeId?: number;
    transactionValue: number;
    inclusionDate: Date;
    transactionDate: Date;
    transactionDescription: string;
    capitalization?: boolean;
    balanceBeforeTransaction?: number;
    accountTransfer?: boolean;
}

export class User {
    userId: string;
    userName: string;
    password: string;
    loginEmail: string;
    token: string;
}
